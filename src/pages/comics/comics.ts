import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import{ComicService} from '../../app/services/comic.service';
import{DetailsPage} from '../details/details';

@Component({
  selector: 'comics',
  templateUrl: 'comics.html'
})
export class ComicsPage {
  items: any;

  constructor(public navCtrl: NavController, private comicService:ComicService) {

  }

  ngOnInit(){
    // limit 90
    this.getPost(90);
  }

  getPost(limit){
    this.comicService.getPosts(limit).subscribe(response => {
      this.items = response.data.results;
    });
  }

  viewItem(item){
    this.navCtrl.push(DetailsPage, {item:item});
  }

  getItems(ev: any){
    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    //if(val.trim() == ''){
    if(val==''){
      this.getPost(90);
    }
  }

}
