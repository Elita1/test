import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/Rx';
import CryptoJS from 'crypto-js';

@Injectable()
export class ComicService{
  http:any;
  baseUrl: String;
  publicKey: String;
  privateKey: String;
  hash: String;

  constructor(http:Http){
    this.http = http;
    this.baseUrl = 'https://gateway.marvel.com/v1/public/comics?'
    this.publicKey = 'b3fd48067a4581abc8be5ac50a9f883e';
    this.privateKey = 'd62d2ee545e9cbd68d468abafdf3500c499a4b3e';
    this.hash = CryptoJS.MD5('1'+this.privateKey+this.publicKey);
  }

  getPosts(limit){
    //https://gateway.marvel.com/v1/public/comics?limit=10&ts=1&apikey=b3fd48067a4581abc8be5ac50a9f883e&hash=634e7add42050ffa197e05799290e3f3
    return this.http.get(this.baseUrl+'limit='+limit+'&ts=1&apikey='+this.publicKey+'&hash='+this.hash).map(res => res.json());
  }

}
